from rest_framework import serializers

from blog.models import Article


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = (
            'id', 'author', 'title', 'slug', 'subtitle', 'image', 'content', 'meta',
            'published_date')

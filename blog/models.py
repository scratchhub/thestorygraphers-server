from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _

from thestorygraphers.settings import AUTH_USER_MODEL


class Article(models.Model):
    """
    Model describing article.
    """
    author = models.ForeignKey(AUTH_USER_MODEL)
    title = models.CharField(max_length=140)
    slug = models.SlugField()
    subtitle = models.CharField(max_length=254, blank=True, null=True)
    image = models.ImageField(upload_to='uploads/', default='uploads/logo.jpg')
    content = models.TextField()
    meta = models.TextField(blank=True, null=True)
    last_edited = models.DateTimeField(default=timezone.now)
    published_date = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = _('article')
        verbose_name_plural = _('articles')

    def get_absolute_url(self):
        return "/%s/" % urlquote(self.slug)

    def publish(self):
        """
        Publishes the article with timestamp.
        """
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from blog.models import Article
from blog.serializers import ArticleSerializer


# Create your views here.
class ArticleListView(generics.ListCreateAPIView):
    """
    View all Article or add new.
    """
    queryset = Article.objects.all().order_by('-published_date')
    serializer_class = ArticleSerializer


class ArticleView(APIView):
    """
    Retrieve or Delete an Article.
    """

    def get(self, request, slug, format=None):
        article = get_object_or_404(Article, slug=slug)
        article_serializer = ArticleSerializer(article)
        return Response(article_serializer.data)

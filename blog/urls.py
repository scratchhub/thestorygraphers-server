from django.conf.urls import url
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns

from blog import views
from thestorygraphers import settings

urlpatterns = [
                  url(r'^api/articles/$', views.ArticleListView.as_view()),
                  url(r'^api/article/(?P<slug>[\w-]+)/$', views.ArticleView.as_view()),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns)

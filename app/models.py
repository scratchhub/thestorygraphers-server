from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _

from thestorygraphers.settings import AUTH_USER_MODEL


class About(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    about_desc = models.TextField(_('About Description'))
    publish_date = models.DateTimeField(_('Publish Date'), default=timezone.now)

    def __str__(self):
        return self.about_desc


class Photograph(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    image = models.ImageField(upload_to='uploads')
    caption = models.CharField(_('Caption'), max_length=140)
    slug = models.SlugField()
    publish_date = models.DateTimeField(_('Publish Date'), default=timezone.now)

    class Meta:
        verbose_name = _('photograph')
        verbose_name_plural = _('photographs')

    def get_absolute_url(self):
        return "/%s/" % urlquote(self.slug)

    def __str__(self):
        return self.caption


class Video(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    thumbnail = models.ImageField(_('Thumbnail (Landscape Aspect Ratio)'), upload_to='uploads')
    youtube_code = models.CharField(_('Youtube Code'), max_length=11)
    caption = models.CharField(_('Caption'), max_length=140)
    slug = models.SlugField()
    publish_date = models.DateTimeField(_('Publish Date'), default=timezone.now)

    class Meta:
        verbose_name = _('video')
        verbose_name_plural = _('videos')

    def get_absolute_url(self):
        return "/%s/" % urlquote(self.slug)

    def __str__(self):
        return self.caption


class Team(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL)
    thumbnail = models.ImageField(_('Thumbnail (Square Aspect Ratio)'), upload_to='uploads')
    index_number = models.IntegerField(_('Index Number(order of display)'))
    name = models.CharField(_('Name'), max_length=140)
    description = models.TextField()
    publish_date = models.DateTimeField(_('Publish Date'), default=timezone.now)

    class Meta:
        verbose_name = _('member')
        verbose_name_plural = _('members')

    def __str__(self):
        return self.name


class Contact(models.Model):
    fullname = models.CharField(max_length=254)
    email = models.EmailField()
    message = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.fullname

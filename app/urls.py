from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from app import views

urlpatterns = [
    url(r'^api/team/$', views.TeamListView.as_view()),
    url(r'^api/about/$', views.AboutView.as_view()),
    url(r'^api/photos/$', views.PhotoListView.as_view()),
    url(r'^api/recentphotos/$', views.PhotoRecentListView.as_view()),
    url(r'^api/videos/$', views.VideoListView.as_view()),
    url(r'^api/recentvideos/$', views.VideoRecentListView.as_view()),
    url(r'^api/contact/$', views.ContactView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

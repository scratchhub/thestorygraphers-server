from rest_framework import serializers

from app.models import Team, About, Photograph, Video, Contact


class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = About
        fields = ('id', 'about_desc')


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ('id', 'thumbnail', 'index_number', 'name', 'description')


class PhotographSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photograph
        fields = ('id', 'image', 'caption', 'slug')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('id', 'thumbnail', 'youtube_code', 'caption', 'slug')


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ('fullname', 'email', 'message')

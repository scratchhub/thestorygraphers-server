from rest_framework import generics

from app.models import Team, About, Photograph, Video, Contact
from app.serializers import TeamSerializer, AboutSerializer, PhotographSerializer, VideoSerializer, ContactSerializer


class AboutView(generics.ListAPIView):
    queryset = About.objects.all().order_by('-publish_date')[:1]
    serializer_class = AboutSerializer


class TeamListView(generics.ListAPIView):
    """
    Fetch Team Endpoint.
    """
    queryset = Team.objects.all().order_by('index_number')
    serializer_class = TeamSerializer


class PhotoListView(generics.ListAPIView):
    """
    Fetch Photos Endpoint.
    """
    queryset = Photograph.objects.all().order_by('-publish_date')
    serializer_class = PhotographSerializer


class PhotoRecentListView(generics.ListAPIView):
    """
    Fetch Photos Endpoint.
    """
    queryset = Photograph.objects.all().order_by('-publish_date')[:4]
    serializer_class = PhotographSerializer


class VideoListView(generics.ListAPIView):
    """
    Fetch Videos Endpoint.
    """
    queryset = Video.objects.all().order_by('-publish_date')
    serializer_class = VideoSerializer


class VideoRecentListView(generics.ListAPIView):
    """
    Fetch Videos Endpoint.
    """
    queryset = Video.objects.all().order_by('-publish_date')[:4]
    serializer_class = VideoSerializer


class ContactView(generics.CreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

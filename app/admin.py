from django.contrib import admin

from app.models import Team, Video, Photograph, About, Contact


class PhotographAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("caption",)}


class VideoAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("caption",)}


admin.site.register(Team)
admin.site.register(About)
admin.site.register(Photograph, PhotographAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Contact)
